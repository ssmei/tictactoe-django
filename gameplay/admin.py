from django.contrib import admin

# Register your models here.
from .models import Game, Move


@admin.register(Game)  # Powerful tool for custom admin view
class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_player', 'second_player', 'status')  # Creates table, wow
    list_editable = ('status',)  # Takes tuple as and argument


admin.site.register(Move)
